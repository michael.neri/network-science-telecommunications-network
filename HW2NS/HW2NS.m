set(0,'defaultTextInterpreter','latex') 
load('A.mat')
% show the original matrix
figure
spy(A)
title('Original matrix')
N = size(A,1);
c = 0.85;
% NOTE : If we inspect the  original adjancency matrix, we can notice that
% it is already sorted in community because of the structural propriety of the network

%% extract eigensystem info

% Laplacian function
d = full(sum(A));
Di = spdiags(1./sqrt(d'),0,N,N);
L = speye(N) - Di*A*Di;

% extract and plot eigenvalues
la = eigs(L,N);                                                            % Eigenvalues of the normalized Laplacian
figure
plot(la,'.')
grid
title('Eigenvalues (of the normalized Laplacian)')

% extract eigenvectors
[V,DD] = eigs(L,4,'SA');
Vv = Di*V;                                                                 % normalize eigenvectors
v1 = Vv(:,2)/norm(Vv(:,2));                                                % Fiedler's vector. We use these eigenvector also for the sweep. 
v2 = Vv(:,3)/norm(Vv(:,3)); 
%% RANKING 

%% Extracting PageRank's values
[r,s,eig] = pageRank(A,zeros(N,1));

%% SimRank
% Since SimRank is similar to PageRank (stochastic process) we only have to
% run PageRank using as a seed node each node of the graph.
disp(['Evaluating SimRank Matrix..']);
tic
simrank = zeros(N,N);
for i = 1:N
    simrank(:,i) = pageRank(A,i);
end
toc
%% Community Detection
% --------------------------------------------------
%% PageRank-nibble approach (Divinding into 2 comm.)

% compute the rankink values r1 with teleport set S={i}
% by exploiting Matlab's linear system solution A\b
disp(['PageRank-nibble ..']);
tic
i = 900;
r1 = simrank(:,i);
% sweep wrt the ordering identified by r1
[u1s,pos2] = sort(r1,'descend');
Nmax = find(u1s>0,1,'last'); % discard nodes with 0 values (never used in Push)
[conduct,D] = conductance(A(pos2,pos2)); 
conduct = conduct(1:Nmax-1);

% plot the conductance sweep
figure
plot(conduct)
grid
title('PageRank-nibble approach (exact)')

% identify the minimum -> threshold
[~,mpos2] = min(conduct);
threshold2 = mean(u1s(mpos2:mpos2+1));

% identify communities
posC1 = sort(pos2(1:mpos2));
C1 = zeros(N,1);
C1(posC1) = 1;
C1(C1 == 0) = NaN;


posC2 = sort(pos2(mpos2+1:end));
C2 = zeros(N,1);
C2(posC2) = -1;
C2(C2 == 0) = NaN;

% Show the two partitions
figure
plot(C1,'x','Color',[0 75 135]/255)
hold on
plot(C2,'x','Color',[255,140,0]/255)
plot(i,'xr')
hold off
grid
ylim([-2 2]);
xlabel('ID Node')
ylabel('ID Community')
title('PageRank-nibble approach for two communities')
legend('Core Network','External Network')
disp('PageRank-nibble approach for two communities')
disp(['   Minimum conductance: ' num2str(conduct(mpos2))])
disp(['   # of links: ' num2str(D/2)])
disp(['Community size #1 (Internal Network): ' num2str(mpos2)])
disp(['Community size #2 (External Network): ' num2str(N-mpos2)])
toc

%% sweep wrt the ordering identified by v1 (Spectral Clustering)
disp(['Spectral Clustering..']);
tic
thrCom = 0.26;

% reorder the adjacency matrix
[v1s,pos] = sort(v1);
A1 = A(pos,pos);
conduct = conductance(A1);

indexCluster = find(islocalmin(conduct));
for l = 1:length(indexCluster)
    if conduct(indexCluster(l))>thrCom
       indexCluster(l) = 0;
    end
end

indexCluster = indexCluster(indexCluster>0)';
disp(['# communities = ' num2str(length(indexCluster))]);
% show the conductance measure
figure
plot(conduct,'x-')
grid
hold on;
plot(thrCom*ones(N,1));
hold off;
title('conductance sweep')

% identify the minimum -> threshold
communities = cell(length(indexCluster),1);
nodes = 1:N;
figure
hold on
grid on
for l = 1:length(indexCluster)
    if l == 1
        communities{1} = nodes(1:indexCluster(1));
    else
        communities{l} = nodes(indexCluster(l-1)+1:indexCluster(l));
    end
        C = zeros(N,1);
        C(communities{l}) = 1;
        C(C == 0) = NaN;
        communities{l} = C;
        plot(communities{l}*rand,'.') 
end
title('Communities')
xlabel('ID Node')
ylabel('ID Community')
toc
%% Modularity Metrics
% modularity optimization with 2 communities
Edges = numEdges(A); % number of edges

B = zeros(N,N);      
for i=1:N
    for j=i:N
        B(i,j) = A(i,j) - 1/(2*Edges)*d(i)*d(j);
    end
end
B = B + (B-diag(diag(B)))';
[bv,lambda] = eigs(B);
j = 1;
bi = bv(:,j);
while lambda(j) < 0
    j = j+1;
    b1 = bv(:,j);
end
se = sign(b1); % community membership signalry

for i=1:N
     if se(i) == 0
         se(i) = 1;
     end
end
Q = 1/(4*Edges)*se'*B*se; % modularity
disp(['Value of Modularity with two communities: ' num2str(Q)])
figure
hold on
se_pos = zeros(N,1);
se_neg = zeros(N,1);
for i = 1:N
    if se(i) == 1
        se_pos(i) = 1;
        se_neg(i) = NaN;
    else
        se_pos(i) = NaN;
        se_neg(i) = -1;
    end    
end
plot(se_pos,'x')
plot(se_neg,'x')
ylim([-1.5 1.5])
title(['Modularity Community Detection']);
grid on;
xlabel('ID node')
ylabel('Membership value')
%% Link Prediction
% --------------------------------------
S_kats = KatsAlg(A);
Scn = CommonNeighbourAlg(A);
Sra = ResourceAllocationAlg(A);
Slp = LocalPathAlg(A);
Saa = AdamicAdarAlg(A);
Srwr = RandomWalkwithRestart(A);

% Displaying number of new links added in the network
kats_links = generatingLinks(S_kats,A,Edges);
disp(['Kats number of new added Links: ' num2str(kats_links)]);
Scn_links = generatingLinks(Scn,A,Edges);
disp(['Common Neighbour number of new added Links: ' num2str(Scn_links)]);
Sra_links = generatingLinks(Sra,A,Edges);
disp(['Resource Allocation number of new added Links: ' num2str(Sra_links)]);
Slp_links = generatingLinks(Slp,A,Edges);
disp(['Local Path number of new added Links: ' num2str(Slp_links)]);
Saa_links = generatingLinks(Saa,A,Edges);
disp(['Adamic Adar number of new added Links: ' num2str(Saa_links)]);
Srwr_links = generatingLinks(Srwr,A,Edges);
disp(['Random Walk with Reset number of new added Links: ' num2str(Srwr_links)]);
%% AUC
disp(['Evaluating AUC for each link prediction algorithm..'])
tic
disp(['Setting up the matrix for AUC..']);

Atmp = A;
percentP = 0.20; % ratio of P set
[row,col] = find(A==0);
Inactive = [row col]; % set I
[row,col] = find(A>0);
Probe = [row col]; % set of all active links (P+T)
Probe = Probe(randperm(size(Probe,1),floor(percentP*numEdges(A))),:);

for i = 1:size(Probe,1)
    Atmp(Probe(i,1),Probe(i,2)) = 0;
end
disp(['Starting evaluations..']);
auc_kats = AUC(Probe,Inactive,KatsAlg(Atmp));
auc_cn = AUC(Probe,Inactive,CommonNeighbourAlg(Atmp));
auc_rwr = AUC(Probe,Inactive,RandomWalkwithRestart(Atmp));
auc_slp = AUC(Probe,Inactive,LocalPathAlg(Atmp));
auc_aa = AUC(Probe,Inactive,AdamicAdarAlg(Atmp));
auc_ra = AUC(Probe,Inactive,ResourceAllocationAlg(Atmp));
disp(['AUC kats: ' num2str(auc_kats)]);
disp(['AUC Common Neighbour: ' num2str(auc_cn)]);
disp(['AUC RWR: ' num2str(auc_rwr)]);
disp(['AUC Local Path: ' num2str(auc_slp)]);
disp(['AUC Adamic Adaar: ' num2str(auc_aa)]);
disp(['AUC Resource Allocation: ' num2str(auc_ra)]);
toc
%% Plot AUC
figure()
cat = categorical({'Kats','Common Neighbour','Random Walk with reset','Local Path','Adamic Adar','Resource Allocation'});
aucs=[auc_kats auc_cn auc_rwr auc_slp auc_aa auc_ra];
rects = bar(cat,aucs,0.5);

rects.FaceColor = 'flat';
rects.CData(1,:) = [0 1 0];
rects.CData(2,:) = [0 0 1];
rects.CData(3,:) = [1 0 0];
rects.CData(4,:) = [0.25 0.50 0.25];
rects.CData(5,:) = [0.25 0.75 0];
rects.CData(6,:) = [0.5 0 0.5];

ylabel('AUC values')
title('AUC Comparison of Different Link Prediction Algorithm')
grid on

%% Plotting link prediction probabilities
figure
contour(Scn)
grid on 
title('Neighbour based Technique - Common Neighbour');
ylabel('i');
xlabel('y');

figure
contour(S_kats)
grid on
title('Path based Techinque - Kats');
ylabel('i');
xlabel('y');

figure
contour(Srwr)
grid on
title('Random Walk based Technique - RWR');
ylabel('i');
xlabel('y');

figure
contour(Slp)
grid on
title('Local Path - LP');
ylabel('i');
xlabel('y');

figure
contour(Saa)
grid on
title('Adamic Adar - AA');
ylabel('i');
xlabel('y');

figure
contour(Sra)
grid on
title('Resource Allocation');
ylabel('i');
xlabel('y');
%% Plotting

% Plot of the convergence

ref = (c*abs(eig(2))).^(1:40);
figure
semilogy([s;ref/ref(end)*s(end)]')
grid
legend('power iteration','second eigenvalue')
xlabel('k [iteration \#]')
ylabel('$\|r_k - r_\infty\|$')
title('PageRank convergence to the second highest eigenvalue')

% Plot of the eigenvalues of PageRank
figure
plot(eig,'.')
hold on
plot(exp(2i*pi*(0:0.001:1)))                                               % Eigenvalues are inside this circumference  
hold off
grid
xlim([-1 1])
title('PageRank eigenvalues')

%PageRank Values
figure
plot(r)
grid
hold on
plot(ones(N,1)*0.004)
hold off
legend('PageRank probabilities','Router threshold')
title('PageRank Probabilities')
xlabel('Nodes')
ylabel('$\Pr_{\infty}[node]$')

%% SimRank 3D plot
figure
mesh(simrank)
colorbar
colormap(flipud(winter(5)))
title('SimRank Matrix')
xlabel('i')
ylabel('j')

%% Functions
function [conduct,D] = conductance(A)
    % evaluate the conductance measure
    a = full(sum(triu(A)));                                                % Sum upper triangular part
    b = full(sum(tril(A)));                                                % Sup lower triangular part
    d = a+b;
    D = sum(d);
    assoc = cumsum(d);
    assoc = min(assoc,D-assoc);
    cut = cumsum(b-a);
    conduct_temp = cut./assoc;
    conduct = conduct_temp(1:end-1);
end

% PageRank & SimRank Computation
function [r,s,eig] = pageRank(A,seedNode)
    N = size(A,1);
    q = zeros(N,1);
    M = A*sparse(diag(1./sum(A)));                                         % Definition of the matrix M 
    c = 0.85;
    if (length(seedNode) == 1)
        q(seedNode)=1;    %SimRank
        r = sparse((eye(N)-c*M)/(1-c))\q;                                  % Solving the linear system exactly
        r = r/sum(r);
    else
        q = ones(N,1)/N;  %Normal PageRank
        disp('Computing PageRank - linear system solution')
        tic
        r = sparse((eye(N)-c*M)/(1-c))\q;                                  % Solving the linear system exactly
        r = r/sum(r);
        toc
    
        disp('Computing PageRank - power iteration')                       % Computing PageRank algorithm with 40 iterations
        tic
        p0 = ones(N,1)/N;   
        s = [];
        for k = 1:40
            p00 = p0;
            p0 = c*M*p0+(1-c)*q;                                           % Stochastic process
            p0 = p0/sum(p0);
            s(k) = norm(p0-r)/sqrt(N);
        end
        toc
    
        disp('Extracting PageRank eigenvalues')
        tic
        eig = eigs(M,size(M,1));    
        toc                                                                % Obtaining all the eigenvalues of PageRank
    end  
end

% Script in order to evaluate # of new links in the network
function numberLinks = generatingLinks(S,A,edg)
    temp = A;
    N = size(A,1);
    for i=1:N
        for j=1:N
            if(A(i,j)==0 && rand()<S(i,j))
                temp(i,j)=1;
            end
        end
    end
    numberLinks=sum(sum(temp))-edg;
end

function m = numEdges(adj)

    sl=selfLoops(adj); % counting the number of self-loops

    if issymmetric(adj) && sl==0    % undirected 
        m=sum(sum(adj))/2;
    
    elseif issymmetric(adj) && sl>0 % counting the self-loops only once
        m=(sum(sum(adj))-sl)/2+sl; 

    elseif not(issymmetric(adj))   % directed graph 
        m=sum(sum(adj));
    
    end
end

function sl=selfLoops(adj)
    sl=sum(diag(adj));
end

function SLP = LocalPathAlg(A)
    N = size(A,1);
    beta = 0.8;
    Slp = full(A^2 + beta*A^3);
    for i=1:N
        Slp(i,i) = 0;
        Slp(:,i) = Slp(:,i)/sum(Slp(:,i));
    end
    SLP = Slp;
end

function Skats = KatsAlg(A)
    N = size(A,1);
    % Path based Techinque - Kats
    beta=0.01; %small value for the parameter 

    S_kats=zeros(N,N);
    links=3; % It must be greater or equal than 2
    for i=1:links
        S_kats=full(S_kats+beta^(i).*(A^i)); % formula for Kats
    end
    Skats = S_kats;
end

function SRA = ResourceAllocationAlg(A)
    % resource allocation
    N = size(A,1);
    A_nt = A ./ repmat(sum(A,2),[1,N]); 
    A_nt(isnan(A_nt)) = 0; 
    A_nt(isinf(A_nt)) = 0;
    Sra = full(A * A_nt);  
    for i=1:N
        Sra(i,i) = 0;
        Sra(:,i) = Sra(:,i)/sum(Sra(:,i));
    end
    SRA = Sra;
end

function SCN = CommonNeighbourAlg(A)
    N = size(A,1);
    % Neighbour based Technique - Common Neighbour
    Scn = full(A*A);
    %normalized Scn
    for i=1:N
        Scn(:,i) = Scn(:,i)/sum(Scn(:,i));
    end
    SCN = Scn;
end

function SAA = AdamicAdarAlg(A)
    % Adamic Adar
    N = size(A,1);
    A_nt = A ./ repmat(log(sum(A,2)),[1,N]); 
    A_nt(isnan(A_nt)) = 0; 
    A_nt(isinf(A_nt)) = 0;  
    Saa = full(A * A_nt);   
    % normalized Saa
    for i=1:N
        Saa(i,i) = 0;
        Saa(:,i) = Saa(:,i)/sum(Saa(:,i));
    end
    SAA = Saa;
end

function SRWR = RandomWalkwithRestart(A)
    % Random Walk based Technique
    % Using SimRank values, We can define Link Prediction with RWR
    N = size(A,1);
    Srwr = zeros(N,N);
    simrank = zeros(N,N);
    for i = 1:N
        simrank(:,i) = pageRank(A,i);
    end
    
    for i = 1:N
        for j = 1:N
            Srwr(i,j) = simrank(i,j) + simrank(j,i);
        end
        Srwr(i,i) = 0;
    end
    for i=1:N
        Srwr(i,i) = 0;
        Srwr(:,i) = Srwr(:,i)/sum(Srwr(:,i));
    end
    SRWR = Srwr;
end

function result = AUC(Probe,Inactive,S)
    count = 0;
    for p =1:size(Probe,1) 
        for i=1:size(Inactive,1)
            if S(Probe(p,1),Probe(p,2))>S(Inactive(i,1),Inactive(i,2))
                count = count+1;
            end
        end
    end
    result = count/(size(Inactive,1)*size(Probe,1));
end