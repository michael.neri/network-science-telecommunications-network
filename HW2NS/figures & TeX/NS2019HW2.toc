\babel@toc {english}{}
\contentsline {section}{\numberline {1}Abstract}{3}{section.1}
\contentsline {section}{\numberline {2}Ranking}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}PageRank}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Power Iteration}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Solving Linear Equation}{5}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}SimRank}{5}{subsection.2.2}
\contentsline {section}{\numberline {3}Community Detection}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}PageRank nibble}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Spectral Clustering}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Modularity}{7}{subsection.3.3}
\contentsline {section}{\numberline {4}Link Prediction}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Neighbour based Techniques}{9}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Common Neighbour}{9}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Adamic Adar}{9}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Resource Allocation}{10}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Path based Techniques}{10}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Kats}{10}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Local Path}{10}{subsubsection.4.2.2}
\contentsline {subsection}{\numberline {4.3}Random Walk based Technique}{11}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Random Walk with restart}{11}{subsubsection.4.3.1}
\contentsline {subsection}{\numberline {4.4}AUC}{12}{subsection.4.4}
\contentsline {section}{\numberline {5}References}{13}{section.5}
