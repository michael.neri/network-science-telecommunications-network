load('A.mat');
load('S.mat');
run = 1; % Fixed # of iterations
Atmp = A;
percentP = 0.10; % ratio of P set
[row,col] = find(A==0);
inactive = [row col]; % set I
[row,col] = find(A>0);
probeSet = [row col]; % set of all active links (P+T)
probeSet = probeSet(randperm(size(probeSet,1),floor(percentP*Links)),:);

for i = 1:size(probeSet,1)
    Atmp(probeSet(i,1),probeSet(i,2)) = 0;
end
count = 0;
% Run Link Prediction Algorithm
for p =1:size(probeSet,1) 
    for i=1:size(inactive,1)
        if Slp(probeSet(p,1),probeSet(p,2))>Slp(inactive(i,1),inactive(i,2))
            count = count+1;
        end
    end
end
result = count/(size(inactive,1)*size(probeSet,1));

function m = numEdges(adj)

    sl=0; % counting the number of self-loops

    if issymmetric(adj) && sl==0    % undirected 
        m=sum(sum(adj))/2;
    
    elseif issymmetric(adj) && sl>0 % counting the self-loops only once
        m=(sum(sum(adj))-sl)/2+sl; 

    elseif not(issymmetric(adj))   % directed graph 
        m=sum(sum(adj));
    
    end
end