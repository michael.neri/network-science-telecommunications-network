# Implementation of Telecommunications Network by means of Matlab
Matlab implementation of a Telecommunications Network for the Network Science A.Y. 2019/2020 course @ UNIPD.
# HW1
Creation from scratch of the network and evaluation of network's behaviour such as mean grades, assortativity, clustering coefficients etc.


# HW2 
Community Detection and Link Prediction.

Further considerations (about implementation and mathematical approaches) are written into HW pdf.
