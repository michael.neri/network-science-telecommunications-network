function v = random_connections(m,p,val_p)
% function v = random_connections(m,p,val_p) generates m random values v 
% according to the probability distribution available in vector p.
% It implements the inverse CDF method
%
% p MUST be a probability vector, i.e., having positive entries summing 
% up to one. v is a vector of length m with values belonging to val_p 
% generated according to the distribution p. If val_p is not defined, then
% it is assumed that val_p = 1:length(p).
% 

cdf = cumsum([0, p]); % evaluate the cdf
randval = rand(1,m); % generate m random values
[~, ~, index] = histcounts(randval,cdf); % applies the inverse cdf
if ~exist('val_p')
    v = index;
else
    v = val_p(index);
end
