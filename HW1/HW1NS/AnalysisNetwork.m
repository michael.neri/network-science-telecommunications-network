clc
clear all

load("A.mat");
N = size(A,1);

K = full(sum(A)); 
K = K(K>0); % avoid zero degrees
K_unique = unique(K); % degree samples
pk = histc(K,K_unique)'; % counts occurrencies
pk = pk/sum(pk); % normalize to 1
kmin=11;
disp(['Mean grades <k> = ' num2str(mean(K))]);
bins = conncomp(graph(A)); %get the components of the network
Ngc=max(histc(bins, unique(bins))); %find the dimension of the GC
disp(['Max degree = ' num2str(max(K_unique))]);
disp(['GC = ' num2str(Ngc)]);

D = graph(A);
distancesD = distances(D); %distances between nodes
disp(['Average Distance evaluated :' num2str(mean(mean(distancesD)))]);
disp(['Diameter:' num2str(max(max(distancesD)))]);
K2 = K(K>=kmin); % restrict range
ga = 1+1/mean(log(K2/kmin)); % estimate the exponent
disp(['Gamma ML = ' num2str(ga)])


c = (ga-1)*kmin^(ga-1); % constant c
C = sum(pk(K_unique>=kmin)); % constant C
                      % correction factor taking into account for  
                      % the fractional impact of the degrees k>=kmin
% log binning
klog = 10.^(0:0.1:ceil(log10(max(K_unique)))); % this identifies the intervals
pklog = histc(K,klog)'; % counts occurrencies
pklog = pklog/sum(pklog); % normalize to 1

% fitting in the PDF signal
s1 = C*c*K_unique.^(-ga); 
% fitting log.bin
s2 = C*c/(ga-1)*klog.^(1-ga) *(1-(klog(2)/klog(1))^(1-ga));
%fittind CCDF
s3 = C*c/(ga-1)*K_unique.^(1-ga);

Pk = cumsum(pk,'reverse');

%% Nth-moments of <k> and other parameters
disp(['Edges = ' num2str(sum(K)/2)]);
k_moments = @(n) C*kmin^(n-ga+1)*(N^(n/(ga-1)-1)-1)/(n-ga+1);
disp(['Mean grades moment 2 <k^2> = ' num2str(k_moments(2))]);
disp(['Mean grades moment 3 <k^3> = ' num2str(k_moments(3))]);
disp(['Variance of <k> = ' num2str(abs((k_moments(2)-mean(K)^2)))]);
disp(['Mean distance <d> approximated = ' num2str(log(N)/log(mean(K)))]);
kappa = k_moments(2)/k_moments(1);
disp(['inhomogeneity ratio kappa = ' num2str(kappa)]);
%% Clustering coeff
%%%%%%%%%%%%%%%%%%%%
disp(['Clustering coefficent...'])

clusterCoeff=zeros(N,1);
for i = 1:N    
    if(K(i)>1)
        indexs=find(A(:,i)>0);
        clusterCoeff(i)= sum(sum(A(indexs,indexs)))/(K(i)*(K(i)-1));
    end
end

cf = unique(clusterCoeff); % degree samples
pcf = histc(clusterCoeff,cf)'; % counts occurrences
disp(['Mean clustering coefficient <C> = ' num2str(mean(clusterCoeff))]);
%% Plotting of the power-law
figure(1)
subplot(2,2,1)
plot(K_unique,pk,'.')
grid
xlabel('k')
ylabel('PDF')
title('linear PDF plot')

subplot(2,2,2)
loglog(K_unique,pk,'.');
hold on;
loglog(K_unique,s1);
xlabel('k')
ylabel('PDF')
grid
axis([xlim min(pk/2) 2*max(pk)])
title('logarithmic PDF plot')

subplot(2,2,3)
loglog(K_unique,Pk,'.')
hold on
loglog(K_unique,s3);
hold off
grid
axis([xlim min(Pk/2) 2])
xlabel('k')
ylabel('CCDF')
title('logarithmic CCDF plot')

subplot(2,2,4)
loglog(klog,pklog,'.')
hold on
loglog(klog,s2);
hold off
axis([xlim min(pklog/2) 2*max(pklog)])
grid
xlabel('k')
ylabel('PDF')
title('logarithmic PDF plot (log bins)')

%% Assortativity

% degrees
K = K';

% averages of neighbours
k_tmp = (A*K)./K;

% extract averages for each value of k
u = unique(K);
k_nn = zeros(1,length(u));
for k = 1:length(u)
    k_nn(k) = mean(k_tmp(K==u(k))); 
end

% linear fitting
p = polyfit(log(u'),log(k_nn),1);
disp(['Assortativity factor ' num2str(p(1))])

%% Robustness 

H=graph(A);
failRate=0.01;

fc= 1- 1/(kappa-1);
beta= 1/(3-ga);
%% Robustness
% aimed attacks

disp(['Aimed attacks!'])

attackAnalysis=zeros(N,1);
elementsToBeRemoved=round(N*failRate);
for i = 1:N
    
    bins = conncomp(H); %get the components of the network
    Ngc=max( histc(bins, unique(bins))); %find the dimension of the GC
    attackAnalysis(i) = Ngc/N; %calculate the ratio of the Ngc over the original N

    %find and remove the highest degree node
    maxDegree=max(degree(H));
    indexVect= find(degree(H)==maxDegree, 1);
    H=rmnode(H,indexVect);
end


%Random Attacks
disp(['Random attacks!'])
H=graph(A);
failAnalysis=zeros(100,1);

for i = 1:100
    
    bins = conncomp(H); %get the components of the network
    Ngc=max( histc(bins, unique(bins))); %find the dimension of the GC
    failAnalysis(i) = Ngc/N; %calculate the ratio of the Ngc over the original N
    
    %randomly select node to remove
    for j=1: elementsToBeRemoved
        %indexVect= degrees(H);
        if(numedges(H)>0)
            H=rmnode(H,randi(max(max(H.Edges.EndNodes))));
        end
    end
end

clear H;

%% Plotting assortativity
figure(2)
loglog(K,k_tmp,'g.');
hold on
loglog(u,exp(p(2)+log(u)*p(1)),'r-');
loglog(u,k_nn,'k.');
hold off
grid
xlabel('k')
ylabel('k_{nn}')
title('Assortativity of the Communication Network')

%% Plotting Clustering coefficient distribution
figure(3)
Pcf = cumsum(pcf,'reverse');
semilogy(cf, Pcf./N,'.-')
grid on
xlabel('Value')
ylabel('Count')
title('Clustering coefficent distribution')

%% Plotting robustness
figure(4)
plot(linspace(0,1),failAnalysis,'o-')
hold on 
plot(linspace(0,1,N),attackAnalysis,'-r')
plot([fc fc], [0 1],'--');
hold off
xlabel('f')
ylabel('A_{inf}(f)/A_{inf}(0)')
title('Robustness plot')
legend('Random failures','Attack', 'Breaking point (f_{c})')
