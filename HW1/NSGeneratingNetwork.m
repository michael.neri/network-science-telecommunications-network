%% Coded by Michael Neri for Network Science course HW1 (undirected graph)
clc
clear all

%% Generation of the communication network. At the beginning, there are only 3 interconnected nodes. (routers)

counter = 3;
%parameters of the network
Nmax =4000;
nodeLanMax = 150;
acc = 21;
A = sparse(zeros(Nmax,Nmax));
A(1,2)=1 ; A(1,3)=1; A(2,1)=1; A(3,1)=1; A(3,2)=1; A(2,3)=1; 
p_redundancy = 0.95; %probability of redundancy
branches =45; %number of interconnected LANs
randomconnections_enduser = 2; %add more random in order not to have only hubs
listrouter = [1 2 3];
% This code generates a random network with real world structure
% (Communication Networks). End user nodes can be personal computer, printers, TVCC etc..
% The internal part of the network will be composed by switches and
% routers. It will be a centralised network.

%All the routers are not the same since they develop different medium in
%order to add redundancy

p_routers = rand(1,3);
p_routers = p_routers/sum(p_routers);


%generation of the LANs
for p=1:branches
    
    %adding routers to the LAN
    subroute = random_connections(1,p_redundancy)+1; %number of routers in the LAN of the branch
    %in order to apply HSRP (Hot Standby Router Protocol)
    for router=1:subroute
        counter = counter +1;
        listrouter = [listrouter counter];
        temp = random_connections(1,p_redundancy)+1;
        link = random_connections(temp,p_routers);
        
        while(length(unique(link)) ~= length(link)) %No multilinks allowed and selfloop (prohibited)
            link = random_connections(temp,p_routers);
        end
        
        for k=1:length(link)
            %counter = counter +1 ;
            A(counter,link(k)) = A(counter,link(k)) +1;
            A(link(k),counter) = A(link(k),counter) +1;
        end
        
    end
    if(subroute == 2)
        pri_router = counter-1;
        sec_router = counter;
        %Interconnecting router of the same LAN
        A(pri_router,sec_router) = A(pri_router,sec_router) +1;
        A(sec_router,pri_router) = A(sec_router,pri_router) +1;
    end
    
    %% Adding end user node phase
    nodesLan = random_connections(1,ones(1,nodeLanMax)/nodeLanMax);
    
    for input=1:nodesLan
        link = listrouter(end);
        counter = counter +1;
        
        if(subroute == 2)
            p_LanRouter = rand(1,2);
            p_LanRouter = p_LanRouter/sum(p_LanRouter);
            link = random_connections(1,p_LanRouter);
            if(link == 1)
                link = listrouter(end-1);
            else
                link = listrouter(end);
            end
        end
        
        A(counter,link) = A(counter,link) +1;
        A(link,counter) = A(link,counter) +1;
        
        %% Adding random connections to each end node
        % We use the Barabasi Albert Model concept (preferential
        % Attachments) with initial Attactiveness
        K = full(sum(A));
        p_i = (K+acc)/(2*randomconnections_enduser*(counter-1)+1); %probability of connecting to node i for the new node
        p_i = p_i/sum(p_i);
        nodesToConnectTo = random_connections(randomconnections_enduser,p_i);
        for index = nodesToConnectTo
            if(index~=0 && input ~=index && ismember(index,listrouter)==0 )  %No selfloop and connections to routers
                A(input,index) = A(input,index)+1;
            	A(index,input) = A(index,input)+1;
            end
        end 
        
    end
end 
A = A(1:counter,1:counter);
save("A.mat","A");

%% HW PART FITTING AND POWERLAW RULE







